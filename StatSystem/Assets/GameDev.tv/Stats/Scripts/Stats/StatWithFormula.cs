﻿using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDev.tv.Stats
{
    [System.Serializable]
    public struct StatWithFormula 
    {
        [SerializeField] private StatDefinition stat;
        [SerializeField] private StatFormulaBase formula;
        [SerializeField] private string overrides;

        public StatDefinition Stat => stat;
        public StatFormulaBase Formula => formula;

        public JObject Overrides => string.IsNullOrEmpty(overrides)?new JObject() : JToken.Parse(overrides) as JObject;

    }
}