﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameDev.tv.Stats
{
    /// <summary>
    /// The StatAggregator class is responsible for aggregating and managing statistics.
    /// </summary>
    public class StatAggregator : MonoBehaviour
    {
        /// <summary>
        /// Represents a dictionary of statistics, where each statistic is identified by a StatDefinition
        /// and has multiple values associated with different string keys.
        /// </summary>
        private Dictionary<StatDefinition, Dictionary<string, float>> stats = new Dictionary<StatDefinition, Dictionary<string, float>>();

        /// <summary>
        /// Informs Observers of a change to a specific stat
        /// </summary>
        public event Action<StatDefinition, float> OnStatChanged;

        /// <summary>
        /// Dictionary holding listeners for stat changes.
        /// </summary>
        private Dictionary<StatDefinition, Dictionary<string, Action<StatDefinition, float>>> listeners =
            new Dictionary<StatDefinition, Dictionary<string, Action<StatDefinition, float>>>();

        /// <summary>
        /// Adds a listener for a specific stat.
        /// </summary>
        /// <param name="stat">The stat definition to add the listener to.</param>
        /// <param name="listenerID">The unique identifier for the listener.</param>
        /// <param name="listener">The action to be executed when the stat changes.</param>
        public void AddListener(StatDefinition stat, string listenerID, Action<StatDefinition, float> listener)
        {
            if (!listeners.ContainsKey(stat))
            {
                listeners[stat] = new Dictionary<string, Action<StatDefinition, float>>();
            }
            listeners[stat][listenerID] = listener;
        }

        /// <summary>
        /// Removes a listener from the specified stat.
        /// </summary>
        /// <param name="stat">The stat from which to remove the listener.</param>
        /// <param name="listenerID">The ID of the listener to remove.</param>
        public void RemoveListener(StatDefinition stat, string listenerID)
        {
            if (!listeners.ContainsKey(stat)) return;
            listeners[stat].Remove(listenerID);
        }

        /// <summary>
        /// Notifies all the registered observers for a given stat with the current value.
        /// </summary>
        /// <param name="stat">The stat definition for which the observers need to be notified.</param>
        void NotifyObservers(StatDefinition stat)
        {
            if (!listeners.ContainsKey(stat)) return;
            foreach (var listener in listeners[stat].Values)
            {
                listener.Invoke(stat, GetStat(stat));
            }
        }
        
        /// <summary>
        /// Sets the value of a stat for a specific provider.
        /// </summary>
        /// <param name="stat">The stat definition.</param>
        /// <param name="providerID">The ID of the provider.</param>
        /// <param name="value">The value to set for the stat.</param>
        /// <remarks>
        /// If the given stat is not found in the dictionary, a new entry is created.
        /// The value of the stat for the specified provider is then updated.
        /// </remarks>
        public void SetStat(StatDefinition stat, string providerID, float value)
        {
            if (!stats.ContainsKey(stat))
            {
                stats[stat] = new Dictionary<string, float>();
            }
            stats[stat][providerID] = value;
            OnStatChanged?.Invoke(stat, GetStat(stat));
        }

        /// <summary>
        /// Retrieves the value of the specified stat.
        /// </summary>
        /// <param name="stat">The stat definition to get the value for.</param>
        /// <returns>The sum of all values associated with the specified stat definition. If the stat is not found, 0f is returned.</returns>
        public float GetStat(StatDefinition stat)
        {
            if (!stats.ContainsKey(stat)) return 0f;
            return stats[stat].Values.Sum();
        }

        /// <summary>
        /// Removes a specific <paramref name="stat"/> definition for a given <paramref name="providerID"/>.
        /// </summary>
        /// <param name="stat">The stat to be removed.</param>
        /// <param name="providerID">The ID of the provider associated with the stat.</param>
        public void RemoveStat(StatDefinition stat, string providerID)
        {
            if (!stats.ContainsKey(stat)) return;
            stats[stat].Remove(providerID);
            OnStatChanged?.Invoke(stat, GetStat(stat));
        }
    }
}