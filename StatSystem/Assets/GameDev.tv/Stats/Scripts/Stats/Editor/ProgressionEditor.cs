﻿using System.Collections.Generic;
using System.Linq;
using GameDev.tv.Utils.Scripts.Parameters;
using Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine;

namespace GameDev.tv.Stats.Editors
{
    [CustomEditor(typeof(Progression))]
    public class ProgressionEditor : Editor
    {
        private SerializedProperty formulas;
        private Progression progression;

        private List<StatDefinition> statDefinitions;
        private List<string> statStrings;
        private List<StatFormulaBase> statFormulas;
        private List<string> statFormulaString;
        
        private void OnEnable()
        {
            formulas = serializedObject.FindProperty(nameof(formulas));
            progression = target as Progression;
            statDefinitions = new List<StatDefinition>();
            foreach (var definition in Resources.LoadAll<StatDefinition>(""))
            {
                statDefinitions.Add(definition);
            }
            statStrings = new List<string>();
            foreach (var definition in statDefinitions)
            {
                statStrings.Add(definition.DisplayName);
            }
            statFormulas = new List<StatFormulaBase>();
            foreach (var definition in Resources.LoadAll<StatFormulaBase>(""))
            {
                statFormulas.Add(definition);
            }
            statFormulaString = new List<string>();
            foreach (var definition in statFormulas)
            {
                statFormulaString.Add(definition.name);
            }
        }

        public override void OnInspectorGUI()
        {
            int objectToRemove = -1;
            for (int i = 0; i < formulas.arraySize; i++)
            {
                SerializedProperty item = formulas.GetArrayElementAtIndex(i);
                {
                    SerializedProperty stat = item.FindPropertyRelative("stat");
                    SerializedProperty formula = item.FindPropertyRelative("formula");
                    EditorGUILayout.BeginHorizontal();
                    List<StatDefinition> filteredList = statDefinitions.ToList();
                    List<string> filteredStrings = statStrings.ToList();
                    for (int s = 0; s < formulas.arraySize; s++)
                    {
                        if (s == i) continue;
                        SerializedProperty otherStat = formulas.GetArrayElementAtIndex(s).FindPropertyRelative("stat");
                        StatDefinition otherDefinition = otherStat.objectReferenceValue as StatDefinition;
                        int r = filteredList.IndexOf(otherDefinition);
                        if (r < 0) continue;
                        filteredList.RemoveAt(r);
                        filteredStrings.RemoveAt(r);
                    }
                    int statIndex = filteredList.IndexOf(stat.objectReferenceValue as StatDefinition);
                    int newIndex = EditorGUILayout.Popup(statIndex, filteredStrings.ToArray());
                    if (newIndex>=0)
                    {
                        stat.objectReferenceValue = filteredList[newIndex];
                    }

                    statIndex = statFormulas.IndexOf(formula.objectReferenceValue as StatFormulaBase);
                    newIndex = EditorGUILayout.Popup(statIndex, statFormulaString.ToArray());
                    if (newIndex >= 0)
                    {
                        formula.objectReferenceValue = statFormulas[newIndex];
                    }
                    if (GUILayout.Button("-"))
                    {
                        objectToRemove = i;
                    }

                    EditorGUILayout.EndHorizontal();
                    StatFormulaBase formulaBase = formula.objectReferenceValue as StatFormulaBase;
                    StatDefinition statDefinition = stat.objectReferenceValue as StatDefinition;
                    if (formulaBase)
                    {
                        var style = new GUIStyle
                        {
                            margin = new RectOffset(10, 10, 0, 0)
                        };
                        EditorGUILayout.BeginVertical(style);
                        bool propertyChanged = false;

                        IEnumerable<ParameterOverride> parameterOverrides = formulaBase.GetParameterOverrides();
                        
                        //JObject properties = formulaBase.GetProperties();
                        string overrides = item.FindPropertyRelative("overrides").stringValue;
                        JObject overrideObject = string.IsNullOrEmpty(overrides)? new JObject() :(JObject)JToken.Parse(overrides);
                        // List<string> keys = new List<string>();
                        // foreach (KeyValuePair<string,JToken> pair in properties)
                        // {
                        //     keys.Add(pair.Key);
                        // }

                        style.margin = new RectOffset(20, 20, 0, 0);
                        EditorGUILayout.BeginVertical(style);
                        foreach (ParameterOverride parameterOverride in parameterOverrides)
                        {
                            switch (parameterOverride.ParameterType)
                            {
                                case ParameterType.Int:
                                {
                                    int value = overrideObject.TryGetValue(parameterOverride.ParameterName, out JToken token)
                                        ? token.ToObject<int>()
                                        : parameterOverride.Value.ToObject<int>();
                                    if (parameterOverride.Slider)
                                    {
                                        EditorGUI.BeginChangeCheck();
                                        int newValue = EditorGUILayout.IntSlider(parameterOverride.ParameterName, value, parameterOverride.Min.ToObject<int>(), parameterOverride.Max.ToObject<int>());
                                        if (EditorGUI.EndChangeCheck())
                                        {
                                            overrideObject[parameterOverride.ParameterName] = newValue;
                                            propertyChanged = true;
                                        }
                                    } else
                                    {
                                        EditorGUI.BeginChangeCheck();
                                        int newValue = EditorGUILayout.IntField(parameterOverride.ParameterName, value);
                                        if (EditorGUI.EndChangeCheck())
                                        {
                                            overrideObject[parameterOverride.ParameterName] = newValue;
                                            propertyChanged = true;
                                        }
                                    }
                                    break;
                                }
                                case ParameterType.Float:
                                {
                                    float value = overrideObject.TryGetValue(parameterOverride.ParameterName, out JToken token)
                                        ? token.ToObject<float>()
                                        : parameterOverride.Value.ToObject<float>();
                                    if (parameterOverride.Slider)
                                    {
                                        
                                        EditorGUI.BeginChangeCheck();
                                        float newValue = EditorGUILayout.Slider(parameterOverride.ParameterName, value, parameterOverride.Min.ToObject<float>(), parameterOverride.Max.ToObject<float>());
                                        if (EditorGUI.EndChangeCheck())
                                        {
                                            overrideObject[parameterOverride.ParameterName] = newValue;
                                            propertyChanged = true;
                                        }
                                    }
                                    else
                                    {
                                        EditorGUI.BeginChangeCheck();
                                        float newValue =
                                            EditorGUILayout.FloatField(parameterOverride.ParameterName, value);
                                        if (EditorGUI.EndChangeCheck())
                                        {
                                            overrideObject[parameterOverride.ParameterName] = newValue;
                                            propertyChanged = true;
                                        }
                                    }

                                    break;
                                }
                                case ParameterType.String:
                                {
                                    string value = overrideObject.TryGetValue(parameterOverride.ParameterName, out JToken token)
                                        ? token.ToObject<string>()
                                        : parameterOverride.Value.ToObject<string>();
                                    EditorGUI.BeginChangeCheck();
                                    string newValue = EditorGUILayout.TextField(parameterOverride.ParameterName, value);
                                    if (EditorGUI.EndChangeCheck())
                                    {
                                        overrideObject[parameterOverride.ParameterName] = newValue;
                                        propertyChanged = true;
                                    }
                                    break;
                                }
                                case ParameterType.Bool:
                                {
                                    bool value = overrideObject.TryGetValue(parameterOverride.ParameterName, out JToken token)
                                        ? token.ToObject<bool>()
                                        : parameterOverride.Value.ToObject<bool>();
                                    EditorGUI.BeginChangeCheck();
                                    bool newValue = EditorGUILayout.Toggle(parameterOverride.ParameterName, value);
                                    if (EditorGUI.EndChangeCheck())
                                    {
                                        overrideObject[parameterOverride.ParameterName] = newValue;
                                        propertyChanged = true;
                                    }
                                    break;
                                }
                            }
                        }
                        
                        // foreach (string key in keys)
                        // {
                        //     float value = overrideObject.TryGetValue(key, out JToken token)
                        //         ? token.ToObject<float>()
                        //         : properties[key].ToObject<float>();
                        //     EditorGUI.BeginChangeCheck();
                        //     float newValue = EditorGUILayout.FloatField(key, value);
                        //     if (EditorGUI.EndChangeCheck())
                        //     {
                        //         overrideObject[key] = newValue;
                        //         propertyChanged = true;
                        //     }
                        // }
                        EditorGUILayout.EndVertical();
                        if(propertyChanged) item.FindPropertyRelative("overrides").stringValue = overrideObject.ToString();
                        if (statDefinition)
                        {
                            EditorGUILayout.LabelField(progression.GetStatDescription(statDefinition));
                        }
                        EditorGUILayout.EndVertical();
                    }
                }
            }
            if (objectToRemove >= 0)
            {
                formulas.DeleteArrayElementAtIndex(objectToRemove);
            }
            if(formulas.arraySize<statDefinitions.Count)
            {
                if (GUILayout.Button("Add Formula"))
                {
                    formulas.InsertArrayElementAtIndex(formulas.arraySize);
                    SerializedProperty newStat = formulas.GetArrayElementAtIndex(formulas.arraySize - 1)
                        .FindPropertyRelative("stat");
                    newStat.objectReferenceValue = null;
                }
            }

            if(serializedObject.hasModifiedProperties)
            {
                serializedObject.ApplyModifiedProperties();
                progression.BuildLookup(true);
            }
        }
    }
}