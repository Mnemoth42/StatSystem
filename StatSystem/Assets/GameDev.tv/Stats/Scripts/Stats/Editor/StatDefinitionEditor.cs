﻿using UnityEditor;

namespace GameDev.tv.Stats.Editors
{
    [CustomEditor(typeof(StatDefinition))]
    public class StatDefinitionEditor : Editor
    {
        private SerializedProperty displayNameProperty;
        private SerializedProperty descriptionProperty;
        private StatDefinition statDefinition;
        private void OnEnable()
        {
            displayNameProperty = serializedObject.FindProperty("displayName");
            descriptionProperty = serializedObject.FindProperty("description");
            statDefinition = (StatDefinition)target;
        }
        
        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();
            string newDisplayName = EditorGUILayout.TextField("Display Name", displayNameProperty.stringValue);
            EditorGUILayout.LabelField("Stat Description");
            string newDescription = EditorGUILayout.TextArea(descriptionProperty.stringValue);
            if (EditorGUI.EndChangeCheck() || string.IsNullOrWhiteSpace(newDisplayName))
            {
                if (string.IsNullOrWhiteSpace(newDisplayName)) newDisplayName = statDefinition.name;
                displayNameProperty.stringValue = newDisplayName;
                descriptionProperty.stringValue = newDescription;
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}