using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameDev.tv.Stats
{
    [CreateAssetMenu(fileName = "New Stat Definition", menuName = "GameDev.tv/Stat Definition")]
    public class StatDefinition : ScriptableObject
    {
        [SerializeField] private string displayName;
        [SerializeField] private string description = "A useful stat";


        public string DisplayName => displayName;
        public string Description => description;
        
        private static Dictionary<string, StatDefinition> statLookup;

        public static StatDefinition GetStatByName(string id)
        {
            if (statLookup == null)
            {
                statLookup = new Dictionary<string, StatDefinition>();
                foreach (var definition in Resources.LoadAll<StatDefinition>("Stats"))
                {
                    statLookup[definition.name] = definition;
                }
            }

            if (statLookup.TryGetValue(id, out var def))
            {
                return def;
            }
            else
            {
                Debug.Log($"Attempted to find Stat with name {id}, but none are found in a Resources folder");
                return null;
            }
        }
    }
}
