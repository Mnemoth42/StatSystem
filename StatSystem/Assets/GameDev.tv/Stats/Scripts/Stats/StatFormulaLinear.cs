﻿using System.Collections.Generic;
using GameDev.tv.Utils.Scripts.Parameters;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDev.tv.Stats
{
    [CreateAssetMenu(fileName = "Linear", menuName = "GameDev.tv/Stat Formulas/Linear", order = 0)]
    public class StatFormulaLinear : StatFormulaBase
    {
        [SerializeField] private float level1 = 10f;
        [SerializeField] private float level100 = 100f;


        public override float GetValue(int level)
        {
            float delta = (level100 - level1)/99f;
            return level1 + delta * (level - 1);
        }
        

        public override JObject GetProperties()
        {
            JObject result = new JObject
            {
                ["Level 1"] = level1,
                ["Level 100"] = level100
            };
            return result;
        }

        public override void SetProperties(JObject properties)
        {
            if(properties.TryGetValue("Level 1", out JToken value))
            {
                level1 = value.ToObject<float>();
            }

            if (properties.TryGetValue("Level 100", out value))
            {
                level100 = value.ToObject<float>();
            }
        }

        public override IEnumerable<ParameterOverride> GetParameterOverrides()
        {
            yield return new ParameterOverride()
            {
                ParameterName = "Level 1",
                ParameterType = ParameterType.Float,
                Value = level1,
                Min = 0f,
                Max = 1000f,
                Slider = true
            };
            yield return new ParameterOverride()
            {
                ParameterName = "Level 100",
                ParameterType = ParameterType.Float,
                Value = level100,
                Min = 0f,
                Max = 100000f,
                Slider = true
            };
        }

        public override void SetParameterOverrides(IEnumerable<ParameterOverride> parameterOverrides)
        {
            foreach (ParameterOverride parameterOverride in parameterOverrides)
            {
                switch (parameterOverride.ParameterName)
                {
                    case "Level 1" : level1 = parameterOverride.Value.ToObject<float>();
                        break;
                    case "Level 100" : level100 = parameterOverride.Value.ToObject<float>();
                        break;
                }
            }
        }
    }
}