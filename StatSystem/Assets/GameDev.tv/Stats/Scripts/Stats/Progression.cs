﻿using System.Collections.Generic;
using UnityEngine;

namespace GameDev.tv.Stats
{
    [CreateAssetMenu(fileName="Progression", menuName = "GameDev.tv/Stats/Progression")]
    public class Progression : ScriptableObject
    {
        [SerializeField] private StatWithFormula[] formulas;

        private Dictionary<StatDefinition, StatFormulaBase> formulaLookup;

        public void BuildLookup(bool force=false)
        {
            if (formulaLookup == null || force)
            {
                formulaLookup = new Dictionary<StatDefinition, StatFormulaBase>();
                foreach (var statWithFormula in formulas)
                {
                    if (statWithFormula.Stat == null || statWithFormula.Formula==null) continue;
                    formulaLookup[statWithFormula.Stat] = Instantiate(statWithFormula.Formula);
                    formulaLookup[statWithFormula.Stat].SetProperties(statWithFormula.Overrides);
                }
            }
        }

        public float GetStat(StatDefinition stat, int level, float fallback = 1.0f)
        {
            BuildLookup();
            if (formulaLookup.TryGetValue(stat, out var formula))
            {
                return formula.GetValue(level);
            }
            else
            {
                Debug.LogWarning($"No formula found for stat {stat.DisplayName}");
                return fallback;
            }
        }

        public string GetStatDescription(StatDefinition stat)
        {
            BuildLookup();
            if (formulaLookup.TryGetValue(stat, out var value))
            {
                return value.GetDescription();
            }
            return "";
        }
    }
}