﻿using System;
using UnityEngine;

namespace GameDev.tv.Stats
{
    public interface IModifierSource
    {
        void SetCallback(Action<StatDefinition, string, float> callback);
    }
}