﻿using System.Collections.Generic;
using GameDev.tv.Utils.Scripts.Parameters;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDev.tv.Stats
{
    public abstract class StatFormulaBase : ScriptableObject
    {
        public abstract float GetValue(int level);

        public string GetDescription()
        {
            return
                $"L1={GetValue(1):F0}, L10={GetValue(10):F0}, L50={GetValue(50):F0}, L75={GetValue(75):F0}, L100={GetValue(100):F0}";
        }
        public abstract JObject GetProperties();
        public abstract void SetProperties(JObject properties);

        public abstract IEnumerable<ParameterOverride> GetParameterOverrides();
        public abstract void SetParameterOverrides(IEnumerable<ParameterOverride> parameterOverrides);

    }
}