﻿using System.Collections.Generic;
using GameDev.tv.Utils.Scripts.Parameters;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDev.tv.Stats
{
    [CreateAssetMenu(fileName = "Simple Multiple", menuName = "GameDev.tv/Stat Formulas/Simple Multiple", order = 0)]
    public class StatFormulaSimpleMultiple : StatFormulaBase
    {
        [SerializeField] private float baseAmount = 10;
        [SerializeField] private float amountPerLevel = 10;
        public override float GetValue(int level)
        {
            return baseAmount + amountPerLevel * (level-1);
        }



        public override JObject GetProperties()
        {
            JObject result = new JObject
            {
                ["Base Amount"] = baseAmount,
                ["Amount Per Level"] = amountPerLevel
            };
            return result;
        }

        public override void SetProperties(JObject properties)
        {
            if (properties.TryGetValue("Base Amount", out JToken value))
            {
                baseAmount = value.ToObject<float>();
            }

            if (properties.TryGetValue("Amount Per Level", out value))
            {
                amountPerLevel = value.ToObject<float>();
            }
        }

        public override IEnumerable<ParameterOverride> GetParameterOverrides()
        {
            yield return new ParameterOverride()
            {
                ParameterName = "Base Amount",
                ParameterType = ParameterType.Float,
                Value = baseAmount,
                Min = 0,
                Max = 10000,
                Slider = true
            };
            yield return new ParameterOverride()
            {
                ParameterName = "Amount Per Level",
                ParameterType = ParameterType.Float,
                Value = amountPerLevel,
                Min = 0,
                Max = 10000,
                Slider = true
            };
        }

        public override void SetParameterOverrides(IEnumerable<ParameterOverride> parameterOverrides)
        {
            foreach (ParameterOverride parameterOverride in parameterOverrides)
            {
                switch (parameterOverride.ParameterName)
                {
                    case "Base Amount":
                        baseAmount = parameterOverride.Value.ToObject<float>();
                        break;
                    case "Amount Per Level":
                        amountPerLevel = parameterOverride.Value.ToObject<float>();
                        break;
                }
            }
        }
    }
}