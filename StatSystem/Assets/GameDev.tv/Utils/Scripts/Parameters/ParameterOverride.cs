﻿using Newtonsoft.Json.Linq;

namespace GameDev.tv.Utils.Scripts.Parameters
{
    public enum ParameterType
    {
        Int,
        Float,
        String,
        Bool
    }
    
    public struct ParameterOverride
    {
        public string ParameterName;
        public ParameterType ParameterType;
        public JToken Value;
        public JToken Min;
        public JToken Max;
        public bool Slider;
    }
}